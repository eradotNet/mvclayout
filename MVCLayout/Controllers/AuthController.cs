﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCLayout.Controllers
{
    public class AuthController : Controller
    {
        //
        // GET: /Login/

        [HttpGet]
        public ActionResult Login()
        {
            if ( Session["ID"] == null)
            {
                ViewBag.email = null;
                return View("Login");
            }
            else
            {
                return RedirectToAction("Home", "Home");
            }
           
        }

        //
        // POST: /Login
        [HttpPost]
        public ActionResult Login(FormCollection formData)
        {

            var data = formData;
            var email = formData["email"];
            var pass = formData["password"];

            if (email == "" || pass == "")
            {
                if (email == "")
                {
                    ViewBag.email = null;
                    ViewBag.emailError = "Email Required";
                }

                if (pass == "")
                {
                    ViewBag.email = null;
                    ViewBag.passwordError = "Password Required";
                }

                return View("login");
            }

            Auth auth = new Auth(email, pass);

            if (HttpContext.Session["ID"] != null)
            {
                return RedirectToAction("Home", "Home");
            }

            else
            {
                ViewBag.email = email;
                return View("login");
            }
        }

        //
        // GET: /Logout
        [HttpGet]
        public ActionResult Logout()
        {
            HttpContext.Session["ID"] = null;
            HttpContext.Session["Email"] = null;
            return RedirectToAction("Login", "Auth"); ;
        }
    }
}
