﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVCLayout.Models;
using System.Data.Entity;
namespace MVCLayout.Controllers
{
    public class Auth
    {
        protected LayoutEntities studentContext = new LayoutEntities();

        protected string email = string.Empty;
        protected string password = string.Empty;
        public long ID = 0;

        public Auth(string loginMail = null, string loginPass = null)
        {
            if (loginMail != null)
                this.email = loginMail;

            if (loginPass != null)
                this.password = loginPass;

            this.checkValidUser();

        }

        public Int64 id()
        {
            return this.ID;
        }

        public string Email()
        {
            return this.email;
        }

        public void checkValidUser()
        {

            var auth = this.studentContext.Users.SingleOrDefault(d => d.Email == this.email && d.Password == this.password);

            if (auth != null)
            {
                this.ID = auth.ID;
                HttpContext.Current.Session["Email"] = auth.Email;
                HttpContext.Current.Session["ID"] = auth.ID;
            }

            else
            {
                HttpContext.Current.Session["Email"] = null;
                HttpContext.Current.Session["ID"] = null;
            }

        }
    }
}