﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCLayout.Models;
using System.ComponentModel.DataAnnotations;

namespace MVCLayout.Controllers
{
    [CustomAuthorize]
    public class HomeController : Controller
    {
        LayoutEntities layoutEntities = new LayoutEntities();
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Home()
        {
            return View("Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }



        // GET
        // Call partial view for rendering
        public ActionResult NavbarMenu()
        {
            var objMenu = layoutEntities.Menubars.Where(m => m.UserGroup == "A").ToList();


            return PartialView("_NavbarMenu", objMenu);
        }

        // GET
        // Call partial view for rendering
        public ActionResult SidebarMenu()
        {
            var objMenu = layoutEntities.Menubars.Where(m => m.UserGroup == "A").ToList();


            return PartialView("_SidebarMenu", objMenu);
        }
    }

}