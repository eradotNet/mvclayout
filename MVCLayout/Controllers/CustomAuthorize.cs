﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MVCLayout.Controllers
{
    public class CustomAuthorize: AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (HttpContext.Current.Session["ID"] == null)
            {
                filterContext.Result = new RedirectToRouteResult(new
                RouteValueDictionary(new { controller = "Auth", action = "Login" }));
                
                //base.HandleUnauthorizedRequest(filterContext);
            }

            //if need any further customization
            ////else
            ////{
            ////    filterContext.Result = new RedirectToRouteResult(new
            ////    RouteValueDictionary(new { controller = "Error", action = "AccessDenied" }));
            ////}
        }
    }
}